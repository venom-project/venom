import Venom, { template } from "./src/venom.js";
import VenomComponent from "./src/venom-component.js";
import VenomValue from "./src/venom-value.js";

export default Venom;
export { template, VenomComponent, VenomValue };